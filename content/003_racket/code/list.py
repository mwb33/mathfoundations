class node:
    def __init__(self):
        self.car = None
        self.cdr = None
#List Functions
#Checks
def isNull(L):
    return L==None
def isCons(L):
    return isinstance(L,node)
def isList(L):
    return isNull(L) or isCons(L)
#Actions
def first(L):
    if isCons(L):
        return L.car
    raise error("No first!")
def rest(L):
    if isCons(L):
        return L.cdr
    raise error("No rest!")
def cons(a,L):
    n = node()
    n.car = a
    n.cdr = L
    return n
def length(L):
    if isNull(L):
        return 0
    else:
        return 1+length(rest(L))
    
L = None
L=cons(1,L)
L=cons(2,L)
L=cons(3,L)

print("Is the list null?")
print(isNull(L)) #False
print("Does the list have elements?")
print(isCons(L)) #True
print("Is it a list?")
print(isList(L)) #True
print("What is the first element?")
print(first(L)) #3
print("What is the second element?")
print(first(rest(L))) # 2
print("What is the length?")
print(length(L))
