f = lambda x : x + x
g = lambda y : 2 * y

print(f(7)) #Prints 14
print(g(8)) #Prints 16
print(g(f(10))) #Prints 40