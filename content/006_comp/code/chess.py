def drawChessBoard(M,highlights):
    #Static HTML Stuff
    darkBox="<td style=\"border: 1px solid black\" height=\"40px\" width=\"40px\" bgcolor=\"#F2F2F2\" align=\"center\">"
    lightBox="<td style=\"border: 1px solid black\" height=\"40px\" width=\"40px\" bgcolor=\"white\" align=\"center\">"
    highlightBox="<td style=\"border: 1px solid black\" height=\"40px\" width=\"40px\" bgcolor=\"orange\" align=\"center\">"
    span="<span style=\"font-size:30px;\">"
    queen="&#x2655;"
    Boxes=[lightBox,darkBox]
    rows = len(M)
    cols = len(M[0])
    
    res="<!-- Making drawChessBoard("+str(M)+","+str(highlights)+") -->\n"
    
    res+="<table>"
    res+="<tbody>"
    color=0
    for i in range(0,rows):
        res+="<tr>"
        for j in range(0,rows):
            if highlights[i][j]==1:
                res+=highlightBox
            else:
                res+=Boxes[color%2]
            color+=1
            ##Draw the Queen
            if M[i][j]==1:
                res+=span
                res+=queen
                res+="</span>"
            else:
                res+="&nbsp;"
            ##End
            res+="</td>"
        res+="</tr>"
        if rows%2==0:
            color+=1
    res+="</tbody>"
    res+="</table>"
    return res

'''
B=[
    [0,1,0,0],
    [0,0,0,1],
    [1,0,0,0],
    [0,0,1,0]]
C=[
    [0,0,0,0],
    [0,0,0,0],
    [0,0,0,0],
    [0,0,0,0]]
print(drawChessBoard(B,C))
'''

'''
#Answer for n=5
B=[
[0,0,0,0,1],
[0,1,0,0,0],
[0,0,0,1,0],
[1,0,0,0,0],
[0,0,1,0,0]
]
C=[
[0,0,0,0,0],
[0,0,0,0,0],
[0,0,0,0,0],
[0,0,0,0,0],
[0,0,0,0,0]
]
print(drawChessBoard(B,C))
'''

B=[
[0,0,0,1,0,0,0,0],#8
[0,1,0,0,0,0,0,0],#16
[0,0,0,0,0,0,0,1],#24
[0,0,0,0,0,1,0,0],#32
[1,0,0,0,0,0,0,0],#40
[0,0,1,0,0,0,0,0],#48
[0,0,0,0,1,0,0,0],#56
[0,0,0,0,0,0,1,0],#64
]
C=[
[0,0,0,0,0,0,0,0],#8
[0,0,0,0,0,0,0,0],#16
[0,0,0,0,0,0,0,0],#24
[0,0,0,0,0,0,0,0],#32
[0,0,0,0,0,0,0,0],#40
[0,0,0,0,0,0,0,0],#48
[0,0,0,0,0,0,0,0],#56
[0,0,0,0,0,0,0,0],#64
]
print(drawChessBoard(B,C))
