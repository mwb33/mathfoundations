#Mark Boady
#Drexel University 2020
#Sierpinski Triangle
import turtle
import math
import sys

def equalTriangle(x,y,s):
    turtle.penup()
    turtle.goto(x,y)
    turtle.pendown()
    turtle.fillcolor("yellow")
    turtle.begin_fill()
    for i in range(0,3):
        turtle.forward(s)
        turtle.left(360/3)
    turtle.end_fill()
    return
def equalRTriangle(x,y,s):
    turtle.penup()
    turtle.goto(x,y)
    turtle.pendown()
    turtle.fillcolor("blue")
    turtle.begin_fill()
    for i in range(0,3):
        turtle.forward(s)
        turtle.right(360/3)
    turtle.end_fill()
    return
def serpTriangle(x,y,s):
    if s < 10:
        return
    #Background Color
    equalTriangle(x,y,s)
    #Determine new Size
    newSize = s/2
    serpTriangle(x,y,newSize)
    serpTriangle(x+s/2,y,newSize)
    #Some Trig is needed here
    opposite = newSize/2
    hypotenuse = newSize
    theta = math.asin(opposite/hypotenuse)
    adjacent = hypotenuse*math.cos(theta)
    serpTriangle(x+newSize/2,y+adjacent,newSize)
    #Draw middle Triangle
    equalRTriangle(x+newSize/2,y+adjacent,newSize)
    
    
turtle.tracer(False)
serpTriangle(-200,-200,300)
cv = turtle.getcanvas()
cv.postscript(file="triangle.ps", colormode='color')


turtle.bye()
sys.exit()
